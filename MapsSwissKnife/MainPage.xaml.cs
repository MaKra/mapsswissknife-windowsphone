﻿using System;
using MapsSwissKnife.Resources;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace MapsSwissKnife
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            BuildLocalizedApplicationBar();
        }

        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar();
            
            // app bar entries 
            
            // sensors info
            var sensorsAppBarButton = new ApplicationBarIconButton(new Uri("/Assets/Buttons/sensor.png", UriKind.Relative));
            sensorsAppBarButton.Text = AppResources.AppBarButtonText_Sensors;
            sensorsAppBarButton.Click += OnSensorsIconClicked;
            ApplicationBar.Buttons.Add(sensorsAppBarButton);

            // compass info
            var compassAppBarButton = new ApplicationBarIconButton(new Uri("/Assets/Buttons/compass.png", UriKind.Relative));
            compassAppBarButton.Text = AppResources.AppBarButtonText_Compass;
            compassAppBarButton.Click += OnCompassIconClicked;
            ApplicationBar.Buttons.Add(compassAppBarButton);

            // settings
            var settingsAppBarButton = new ApplicationBarIconButton(new Uri("/Assets/Buttons/settings.png", UriKind.Relative));
            settingsAppBarButton.Text = AppResources.AppBarButtonText_Settings;
            ApplicationBar.Buttons.Add(settingsAppBarButton);

            // menu items
            var aboutAppBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText_About);
            aboutAppBarMenuItem.Click += OnAboutPageClicked;
            ApplicationBar.MenuItems.Add(aboutAppBarMenuItem);
        }

        private void OnCompassIconClicked(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri(string.Format("/Source/CompassPage.xaml"), UriKind.Relative));
        }

        private void OnSensorsIconClicked(object sender, EventArgs e)
        {            
            NavigationService.Navigate(new Uri(string.Format("/Source/SensorsPage.xaml"), UriKind.Relative));
        }

        private void OnAboutPageClicked(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri(string.Format("/Source/AboutPage.xaml"), UriKind.Relative));   
        }
    }
}