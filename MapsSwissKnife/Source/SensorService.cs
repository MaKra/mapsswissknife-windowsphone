﻿using Microsoft.Devices.Sensors;
using System;

namespace MapsSwissKnife.Source
{
    using System.Windows;

    using Microsoft.Phone.Shell;

    class SensorService
    {
        private static SensorService _current;

        private const int SensorsUpdateInterval = 200;  // in ms

        public Motion Motion { get; private set; }
        public Compass Magnetometer { get; private set;}
        public Accelerometer Accelerometer { get; private set; }        
        public Gyroscope Gyroscope { get; private set; }
        

        public static SensorService Current
        { 
            get { return _current ?? (_current = new SensorService()); }
        }      

        private SensorService()
        {
            // create new compass
            if (Compass.IsSupported)
            {
                this.Magnetometer = new Compass();
                System.Diagnostics.Debug.WriteLine("Compass initialized...");
                this.Magnetometer.TimeBetweenUpdates = TimeSpan.FromMilliseconds(SensorsUpdateInterval);
                this.Magnetometer.Start(); // is needed also on map, so we can directly start this                                
                System.Diagnostics.Debug.WriteLine("Compass started");
            }
            
            // create new accelerometer
            if (Accelerometer.IsSupported)
            {
                this.Accelerometer = new Accelerometer();
                this.Accelerometer.TimeBetweenUpdates = TimeSpan.FromMilliseconds(SensorsUpdateInterval);

                System.Diagnostics.Debug.WriteLine("Accelerometer initialized...");
            }

            // create new gyroscope
            if (Gyroscope.IsSupported)
            {
                this.Gyroscope = new Gyroscope();
                this.Gyroscope.TimeBetweenUpdates = TimeSpan.FromMilliseconds(SensorsUpdateInterval);

                System.Diagnostics.Debug.WriteLine("Gyroscope initialized...");
            }

            // create new motion instance
            if (Motion.IsSupported)
            {
                this.Motion = new Motion();
                this.Motion.TimeBetweenUpdates = TimeSpan.FromMilliseconds(SensorsUpdateInterval);

                System.Diagnostics.Debug.WriteLine("Motion object initialized...");
            }

            // if tombstone/dormant, make sure we also disable the sensors
            PhoneApplicationService.Current.Deactivated += this.OnAppDeactivated;

            System.Diagnostics.Debug.WriteLine("Sensor service fully initialized...");
        }

        // if tombstone/dormant, make sure we also disable the sensors
        private void OnAppDeactivated(object sender, DeactivatedEventArgs e)
        {            
            PhoneApplicationService.Current.Deactivated -= this.OnAppDeactivated;

            if (SensorService.Current.Magnetometer != null)
            {
                SensorService.Current.Magnetometer.Stop();
                System.Diagnostics.Debug.WriteLine("Compass stopped");
            }
        }

    }

}
