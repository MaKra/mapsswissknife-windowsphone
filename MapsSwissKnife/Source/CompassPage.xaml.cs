﻿using System;
using MapsSwissKnife.Resources;
using Microsoft.Devices.Sensors;
using Microsoft.Phone.Controls;

namespace MapsSwissKnife.Source
{
    public partial class CompassPage : PhoneApplicationPage
    {
        public CompassPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (Compass.IsSupported)            
                SensorService.Current.Magnetometer.CurrentValueChanged += OnCurrentCompassValueChanged;
        }

        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            if (Compass.IsSupported)
                SensorService.Current.Magnetometer.CurrentValueChanged -= OnCurrentCompassValueChanged;
        }

        private void OnCurrentCompassValueChanged(object sender, SensorReadingEventArgs<CompassReading> e)
        {
            var value = e.SensorReading;

            this.Dispatcher.BeginInvoke(() =>
            {
                // set data text
                AccuracyLabel.Text = String.Format("±{0}°",value.HeadingAccuracy);

                if (value.HeadingAccuracy >= 20)  // defined treshold for recalibration suggestions
                {
                    AccuracyLabel.Text += " (" + AppResources.CompassRecalibrationNeeded + ")";
                }

                TrueHeadingLabel.Text = String.Format("{0}°", (360-value.TrueHeading).ToString("0.0"));
                MagneticHeadingLabel.Text = String.Format("{0}°", (360-value.MagneticHeading).ToString("0.0"));

                var rawMagnetometerReading = value.MagnetometerReading;
                RawMagneticHeadingLabel.Text = rawMagnetometerReading.X.ToString("0.00") + " (x) / "
                                               + rawMagnetometerReading.Y.ToString("0.00") + " (y) / "
                                               + rawMagnetometerReading.Z.ToString("0.00") + " (z)";
                // rotate compass image
                CompassRotation.Angle = -value.TrueHeading;
            });

        }
    }
}