﻿using System.Device.Location;
using Windows.Devices.Geolocation;

namespace MapsSwissKnife.Source
{
    internal class LocationService
    {
        private const int MovementTreshold = 10; // Treshold for GPS movement before sending event in meters

        private static LocationService _current;

        public Geolocator LocationEngine { get; private set; }

        public GeoCoordinate CurrentPosition
        {
            get 
            {
                return this.CurrentCoordinate == null ? null : new GeoCoordinate(this.CurrentCoordinate.Latitude, this.CurrentCoordinate.Longitude);
            }
        }
    
        public Geocoordinate CurrentCoordinate { get; private set; }

        public static LocationService Current
        { 
            get { return _current ?? (_current = new LocationService()); }
        }

        private LocationService()
        {
            // create and init location watcher
            this.LocationEngine = new Geolocator
            {
                DesiredAccuracy = PositionAccuracy.High,
                MovementThreshold = LocationService.MovementTreshold
            };

            this.LocationEngine.StatusChanged += OnPositionStatusChanged;
            this.LocationEngine.PositionChanged += OnPositionChanged;

            System.Diagnostics.Debug.WriteLine("Location service initialized...");
        }      

        // location internal handling for position changes (non ui relevant stuff)
        private void OnPositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            // save last coordinate we got from the service            
            this.CurrentCoordinate = args.Position.Coordinate;
        }

        // location serviec internal handling for status changes (non ui relevant stuff)
        private void OnPositionStatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {            

        }
    }    
}
