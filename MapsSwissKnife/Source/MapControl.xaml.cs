﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Imaging;
using Microsoft.Devices.Sensors;
using Microsoft.Phone.Maps.Controls;
using System.Device.Location;
using System.Windows.Controls;
using Windows.Devices.Geolocation;

namespace MapsSwissKnife.Source
{
    public partial class MapControl : UserControl
    {
        public Map MapEngine { get; private set; }
    
        public bool IsTracking { get; private set; }

        public bool IsHeadsUp { get; private set; }

        public MapOverlay MyPositionMarker { get; private set; }

        private const double DefaultZoomLevel = 14;   // default zoomlevel for maps startup of resetting position/tracking


        public MapControl()
        {
            InitializeComponent();

            // take best possible position on startup. if location service has already something, tkae this, otherwise take default center
            var center = LocationService.Current.CurrentPosition ?? new GeoCoordinate(52.52, 13.40);

            // if location service has already a position available when starting up UI
            var positionAvailable = LocationService.Current.CurrentPosition != null;

            // create and init map engine
            this.MapEngine = new Map
                                 {
                                     Center = center,
                                     ZoomLevel = MapControl.DefaultZoomLevel,
                                     ColorMode = MapColorMode.Light,
                                     PedestrianFeaturesEnabled = true,
                                     LandmarksEnabled = true
                                 };

            LayoutRoot.Children.Insert(0, this.MapEngine);

            // create my position layer
            var positionImage = new Image
                                    {
                                        Source = new BitmapImage(new Uri("/Assets/positionOn.png", UriKind.RelativeOrAbsolute)),
                                        Visibility = positionAvailable ? Visibility.Visible : Visibility.Collapsed
                                    };

            this.MyPositionMarker = new MapOverlay
                                        {
                                            Content = positionImage,
                                            GeoCoordinate = center
                                        };

            var positionLayer = new MapLayer
                                    {
                                        this.MyPositionMarker
                                    };

            this.MapEngine.Layers.Add(positionLayer);
            
            // activate my position button if position available
            this.MyPositionButton.IsEnabled = positionAvailable;

            // get notified and update ui when GPS update happens
            LocationService.Current.LocationEngine.StatusChanged += this.OnPositionStatusChanged;
            LocationService.Current.LocationEngine.PositionChanged += this.OnPositionChanged;

            if (Compass.IsSupported)
            {
                // get notified and update ui when compass update happens
                SensorService.Current.Magnetometer.CurrentValueChanged += this.OnCurrentCompassValueChanged;

                // set heads up or north up when clicking on image
                this.CompassNeedleImage.Tap += this.OnCompassClicked;

                // set heads up by default when compass is available
                this.IsHeadsUp = true;
            }
            else
            {
                // set north up by default when compass is not available
                this.IsHeadsUp = false;
            }

            // get notified and update ui when map pitch was changed
            PitchSlider.ValueChanged += this.OnPitchValueChanged;
            
            // default on startup: tracking mode
            this.IsTracking = true;            
        }

        private void OnCompassClicked(object sender, System.Windows.Input.GestureEventArgs e)
        {
            if (this.IsHeadsUp) // disable heads up mode, reset angle of map, switch to north up mode
            {
                this.IsHeadsUp = false;
                this.MapEngine.Heading = 0.0;
            }
            else
            {
                this.IsHeadsUp = true;
            }
        }
       
        private void OnCurrentCompassValueChanged(object sender, SensorReadingEventArgs<CompassReading> e)
        {
            // update compass needle on map
            this.Dispatcher.BeginInvoke(() =>
            {               
                CompassNeedleRotation.Angle = -e.SensorReading.TrueHeading;

                if (this.IsHeadsUp) // is we align map north up, rotate map now
                {
                    MapEngine.Heading = e.SensorReading.TrueHeading;
                }
            });
        }

        private void OnPitchValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.MapEngine.Pitch = e.NewValue;
        }

        private void OnPositionStatusChanged(Geolocator sender, StatusChangedEventArgs args)
        {
            var status = "";

            switch (args.Status)
            {
                case PositionStatus.Disabled:
                    status = "disabled";
                    break;
                case PositionStatus.Initializing:
                    // the geolocator started the tracking operation
                    status = "initializing";
                    break;
                case PositionStatus.NoData:
                    //the location service was not able to acquire the location
                    status = "no data";
                    break;
                case PositionStatus.Ready:
                    status = "ready";
                    break;
                case PositionStatus.NotAvailable:
                    status = "not available";
                    break;
                case PositionStatus.NotInitialized:
                    status = "not initialized";
                    break;
            }            

            Dispatcher.BeginInvoke(() =>
            {
                this.StatusTextbox.Text = status;
            });
        }

        private void OnPositionChanged(Geolocator sender, PositionChangedEventArgs args)
        {
            if (args.Position == null || args.Position.Coordinate == null)  // sanity check
                return;

            Dispatcher.BeginInvoke(() =>
            {
                // update text boxes
                this.CoordinatesTextbox.Text = args.Position.Coordinate.Latitude.ToString("0.000000")+" (lat)" 
                                               + ", " 
                                               + args.Position.Coordinate.Longitude.ToString("0.000000")+ " (lng)"
                                               + ", "
                                               + args.Position.Coordinate.Altitude+" m (alt)";
                
                this.AccuracyTextbox.Text = args.Position.Coordinate.Accuracy.ToString(CultureInfo.InvariantCulture)+" m";

                if (args.Position.Coordinate.Speed != null)
                    this.SpeedTextbox.Text = args.Position.Coordinate.Speed.Value.ToString("0.00") + " km/h";

                this.SourceTextbox.Text = args.Position.Coordinate.PositionSource.ToString();

                // set position dot to visible, if we get valid GPS signal
                var image = this.MyPositionMarker.Content as Image;
                if (image != null && image.Visibility == Visibility.Collapsed)
                {
                    image.Visibility = Visibility.Visible;
                }

                // update position dot. we take the position we get via arguments and not via singelton
                // location service instance, because we don't know if location service also porcessed the values and saved it
                var retrievedPosition = new GeoCoordinate(args.Position.Coordinate.Latitude, args.Position.Coordinate.Longitude);
                this.MyPositionMarker.GeoCoordinate = retrievedPosition;

                // also activate my position button when we got a valid position (for toggelig tracking)
                if (this.MyPositionButton.IsEnabled == false)
                {
                    this.MyPositionButton.IsEnabled = true;
                }

                // jump to location if in tracking mode, with keeping current zoomlevel
                if (this.IsTracking)
                {
                    this.JumpToCurrentPosition(this.MapEngine.ZoomLevel);
                }
            });
        }

        private void JumpToCurrentPosition(double zoomLvl = MapControl.DefaultZoomLevel, MapAnimationKind animation = MapAnimationKind.Parabolic)
        {
            if (LocationService.Current.CurrentPosition != null)
            {
                this.MapEngine.SetView(
                    LocationService.Current.CurrentPosition,
                    zoomLvl,
                    this.MapEngine.Heading,
                    this.MapEngine.Pitch,
                    animation);
            }
        }

        private void OnMapModeSelected(object sender, RoutedEventArgs e)
        {
            this.MapEngine.CartographicMode = MapCartographicMode.Road;
        }

        private void OnSatelliteModeSelected(object sender, RoutedEventArgs e)
        {
            this.MapEngine.CartographicMode = MapCartographicMode.Aerial;
        }

        private void OnTerrainModeSelected(object sender, RoutedEventArgs e)
        {
            this.MapEngine.CartographicMode = MapCartographicMode.Terrain;
        }

        private void OnZoomIn(object sender, RoutedEventArgs e)
        {
            this.MapEngine.ZoomLevel++;
        }

        private void OnZoomOut(object sender, RoutedEventArgs e)
        {
            this.MapEngine.ZoomLevel--;
        }

        private void OnColorModeChanged(object sender, RoutedEventArgs e)
        {
            var tb = sender as ToggleButton;
            if (this.MapEngine != null && tb != null)
            {
                this.MapEngine.ColorMode = tb.IsChecked == true ? MapColorMode.Light : MapColorMode.Dark;
            }
        }

        private void OnLandmarksModeChanged(object sender, RoutedEventArgs e)
        {
            var tb = sender as ToggleButton;

            if (this.MapEngine == null || tb == null || tb.IsChecked == null) 
                return;

            if ( this.MapEngine.LandmarksEnabled != tb.IsChecked )
                this.MapEngine.LandmarksEnabled = (bool) tb.IsChecked;
        }

        private void OnPedestrianModeChanged(object sender, RoutedEventArgs e)
        {
            var tb = sender as ToggleButton;

            if (this.MapEngine == null || tb == null || tb.IsChecked == null)
                return;

            if (this.MapEngine.PedestrianFeaturesEnabled != tb.IsChecked)
                this.MapEngine.PedestrianFeaturesEnabled = (bool)tb.IsChecked;
        }

        private void OnMyPositionClicked(object sender, RoutedEventArgs e)
        {
            if (LocationService.Current.LocationEngine.LocationStatus != PositionStatus.Ready) 
                return;  // if GPS not ready, don't react on event

            this.IsTracking = true;
            JumpToCurrentPosition();  // reset to my position and reset zoom level
        }

    }
}
