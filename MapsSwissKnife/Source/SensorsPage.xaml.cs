﻿using System;
using MapsSwissKnife.Resources;
using Microsoft.Devices.Sensors;
using Microsoft.Phone.Controls;
using Microsoft.Xna.Framework;

namespace MapsSwissKnife.Source
{
    public partial class SensorsPage : PhoneApplicationPage
    {
        private Accelerometer _accelerometer;
        private Gyroscope _gyroscope;
        private Compass _magnetometer;
        private Motion _motion;

        public SensorsPage()
        {
            InitializeComponent();           
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            this.StartAllSensors();          
        }
     
        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            this.StopAllSensors();                        
        }

        private void StartAllSensors()
        {
            // activate sensors            

            if (Compass.IsSupported)
            {
                this._magnetometer = SensorService.Current.Magnetometer;
                this._magnetometer.CurrentValueChanged += OnMagnetometerValueChanged;
            }

            if (Accelerometer.IsSupported)
            {
                this._accelerometer = SensorService.Current.Accelerometer;
                this._accelerometer.Start();
                System.Diagnostics.Debug.WriteLine("Accelerometer started");
                this._accelerometer.CurrentValueChanged += OnAccelerometerValueChanged;
            }

            if (Gyroscope.IsSupported)
            {
                this._gyroscope = SensorService.Current.Gyroscope;
                this._gyroscope.Start();
                System.Diagnostics.Debug.WriteLine("Gyroscope started");
                this._gyroscope.CurrentValueChanged += OnGyroscopeValueChanged;
            }

            if (Motion.IsSupported)
            {
                this._motion = SensorService.Current.Motion;
                this._motion.Start();
                System.Diagnostics.Debug.WriteLine("Motion sensor started");
                this._motion.CurrentValueChanged += OnMotionValueChanged;
            }
        }

        private void StopAllSensors()
        {
            // Unbind events and deactivate sensors. 
            // Compass is needed outside this page and therefore just unbound but not stopped.

            if (Compass.IsSupported)
            {
                this._magnetometer.CurrentValueChanged -= OnMagnetometerValueChanged;
            }

            if (Accelerometer.IsSupported)
            {
                this._accelerometer.CurrentValueChanged -= OnAccelerometerValueChanged;
                this._accelerometer.Stop();
                System.Diagnostics.Debug.WriteLine("Accelerometer stopped");
            }

            if (Gyroscope.IsSupported)
            {
                this._gyroscope.CurrentValueChanged -= OnGyroscopeValueChanged;
                this._gyroscope.Stop();
                System.Diagnostics.Debug.WriteLine("Gyroscope stopped");
            }

            if (Motion.IsSupported)
            {
                this._motion.CurrentValueChanged -= OnMotionValueChanged;
                this._motion.Stop();
                System.Diagnostics.Debug.WriteLine("Motion sensor stopped");
            }
        }

        private void OnAccelerometerValueChanged(object sender, SensorReadingEventArgs<AccelerometerReading> sensorReadingEventArgs)
        {
            var value = sensorReadingEventArgs.SensorReading.Acceleration;

            this.Dispatcher.BeginInvoke(() =>
            {
                // In unit "g", including gravity
                // See: http://msdn.microsoft.com/en-us/library/windowsphone/develop/microsoft.devices.sensors.accelerometerreading.acceleration(v=vs.105).aspx
                AccelerometerXText.Text = value.X.ToString("0.00") + " g";
                AccelerometerYText.Text = value.Y.ToString("0.00") + " g";
                AccelerometerZText.Text = value.Z.ToString("0.00") + " g";

                // also update orientation of compass, that also uses accelerometer data
                bool isCompassUsingNegativeZAxis = Math.Abs(value.Z) < Math.Cos(Math.PI / 4) && (value.Y < Math.Sin(7 * Math.PI / 4));
                CompassOrientationText.Text = (isCompassUsingNegativeZAxis) ? "portrait mode" : "flat mode";
            });
        }

        private void OnGyroscopeValueChanged(object sender, SensorReadingEventArgs<GyroscopeReading> sensorReadingEventArgs)
        {
            var value = sensorReadingEventArgs.SensorReading;

            this.Dispatcher.BeginInvoke(() =>
            {
                // values are radians per second
                // See: http://msdn.microsoft.com/en-us/library/windowsphone/develop/microsoft.devices.sensors.gyroscopereading(v=vs.105).aspx

                GyroscopeXText.Text = MathHelper.ToDegrees(value.RotationRate.X).ToString("0.00") + "° per second";
                GyroscopeYText.Text = MathHelper.ToDegrees(value.RotationRate.Y).ToString("0.00") + "° per second";
                GyroscopeZText.Text = MathHelper.ToDegrees(value.RotationRate.Z).ToString("0.00") + "° per second";   
            });
        }

        private void OnMotionValueChanged(object sender, SensorReadingEventArgs<MotionReading> e)
        {
            var value = e.SensorReading;

            this.Dispatcher.BeginInvoke(() =>
            {
                // for details and units, see:
                // http://msdn.microsoft.com/en-us/library/windowsphone/develop/microsoft.devices.sensors.motionreading(v=vs.105).aspx

                // determined values for attitude (orientation in 3D space)
                MotionAttitudeZText.Text = MathHelper.ToDegrees(value.Attitude.Yaw).ToString("0.00") + "°";
                MotionAttitudeXText.Text = MathHelper.ToDegrees(value.Attitude.Pitch).ToString("0.00") + "°";
                MotionAttitudeYText.Text = MathHelper.ToDegrees(value.Attitude.Roll).ToString("0.00") + "°";

                // same values as we get from gyroscope directly, but without gravity. 
                // so this is the pure acceleration
                MotionAccelerationXText.Text = value.DeviceAcceleration.X.ToString("0.00")+" g";
                MotionAccelerationYText.Text = value.DeviceAcceleration.Y.ToString("0.00")+" g";
                MotionAccelerationZText.Text = value.DeviceAcceleration.Z.ToString("0.00")+" g";

                // graphical acceleration representation
                XAccLine.X2 = XAccLine.X1 + value.DeviceAcceleration.X * 100;
                YAccLine.Y2 = YAccLine.Y1 - value.DeviceAcceleration.Y * 100;
                ZAccLine.X2 = ZAccLine.X1 - value.DeviceAcceleration.Z * 50;
                ZAccLine.Y2 = ZAccLine.Y1 + value.DeviceAcceleration.Z * 50;
            });
        }

        private void OnMagnetometerValueChanged(object sender, SensorReadingEventArgs<CompassReading> sensorReadingEventArgs)
        {
            var value = sensorReadingEventArgs.SensorReading;

            this.Dispatcher.BeginInvoke(() =>
            {
                // for values see:
                // http://msdn.microsoft.com/en-us/library/windowsphone/develop/microsoft.devices.sensors.compassreading(v=vs.105).aspx

                CompassAccuracyText.Text = String.Format("±{0}°", value.HeadingAccuracy);

                if (value.HeadingAccuracy >= 20)  // defined treshold for recalibration suggestions
                {
                    CompassAccuracyText.Text += " (" + AppResources.CompassRecalibrationNeeded + ")";
                }

                CompassTrueHeadingText.Text = String.Format("{0}°", (360 - value.TrueHeading).ToString("0.0"));
                CompassMagneticHeadingText.Text = String.Format("{0}°", (360 - value.MagneticHeading).ToString("0.0"));

                var rawMagnetometerReading = value.MagnetometerReading;
                CompassRawReadingXText.Text = rawMagnetometerReading.X.ToString("0.00") + " µT";
                CompassRawReadingYText.Text = rawMagnetometerReading.Y.ToString("0.00") + " µT";
                CompassRawReadingZText.Text = rawMagnetometerReading.Z.ToString("0.00") + " µT";
            });
        }

    }
}